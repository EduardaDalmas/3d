﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BollBehaviourScript : MonoBehaviour
{
    private float x, y, z;
    public float velocity;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        x = this.transform.position.x;
        y = this.transform.position.y;
        z = this.transform.position.z;

        if (Input.GetAxis("Horizontal") > 0)
        {
            this.transform.position = new Vector3(x + velocity,y,z);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            this.transform.position = new Vector3(x - velocity,y,z);
        }

         if (Input.GetAxis("Vertical") > 0)
        {
            this.transform.position = new Vector3(x,y,z + velocity);
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            this.transform.position = new Vector3(x,y,z - velocity);
        }
    }
}
